package com.online.shop.mapper;

import com.online.shop.dto.ChosenProductDTO;
import com.online.shop.dto.CustomerOrderDTO;
import com.online.shop.entities.ChosenProduct;
import com.online.shop.entities.CustomerOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class CustomerOrderMapper {

    @Autowired
    private ChosenProductMapper chosenProductMapper;

    public CustomerOrderDTO map (CustomerOrder customerOrder){
        CustomerOrderDTO customerOrderDTO = new CustomerOrderDTO();
        customerOrderDTO.setCustomerName(customerOrder.getUser().getFullName());
        customerOrderDTO.setOrderID(String.valueOf(customerOrder.getId()));
        List<ChosenProductDTO> chosenProductDTOList = new ArrayList<>();
        for (ChosenProduct chosenProduct : customerOrder.getChosenProducts()) {
            ChosenProductDTO chosenProductDTO = chosenProductMapper.map(chosenProduct);
            chosenProductDTOList.add(chosenProductDTO);
        }
        customerOrderDTO.setChosenProductDTOList(chosenProductDTOList);
        return customerOrderDTO;
    }

}
