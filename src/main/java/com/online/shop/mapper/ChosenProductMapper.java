package com.online.shop.mapper;

import com.online.shop.dto.ChosenProductDTO;
import com.online.shop.dto.ProductDTO;
import com.online.shop.entities.ChosenProduct;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class ChosenProductMapper {

    @Autowired
    private ProductMapper productMapper;

    public ChosenProductDTO map(ChosenProduct chosenProduct){
        ChosenProductDTO chosenProductDTO = new ChosenProductDTO();
        chosenProductDTO.setQuantity(String.valueOf(chosenProduct.getChosenQuantity()));
        ProductDTO productDTO = productMapper.map(chosenProduct.getProduct());
        chosenProductDTO.setProductDTO(productDTO);
        return chosenProductDTO;
    }

}
