package com.online.shop.controller;

import com.online.shop.dto.*;
import com.online.shop.service.CustomerOrderService;
import com.online.shop.service.ProductService;
import com.online.shop.service.ShoppingCartService;
import com.online.shop.service.UserService;
import com.online.shop.validator.ProductValidator;
import com.online.shop.validator.UserValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.io.IOException;
import java.util.List;
import java.util.Optional;

@Controller
public class MainController {

    @Autowired
    private ProductService productService;

    @Autowired
    private UserService userService;

    @Autowired
    private ProductValidator productValidator;

    @Autowired
    private UserValidator userValidator;

    @Autowired
    private ShoppingCartService shoppingCartService;

    @Autowired
    private CustomerOrderService customerOrderService;

    @GetMapping("/addProduct")
    public String addProductPageGet(Model model) {

        ProductDTO productDTO = new ProductDTO();
        model.addAttribute("productDTO", productDTO);
        return "addProduct";
    }

    @PostMapping("/addProduct")
    public String addProductPagePost(@ModelAttribute ProductDTO productDTO, BindingResult bindingResult,
                                     @RequestParam("productImage") MultipartFile multipartFile) throws IOException {
        productValidator.validate(productDTO, bindingResult);
        if (bindingResult.hasErrors()) {
            return "addProduct";
        }
        productService.addProduct(productDTO, multipartFile);
        return "redirect:/addProduct";
    }
    @GetMapping("/home")
    public String homepageGet(Model model){
        List<ProductDTO> productDTOList = productService.getAllProductDTOs();
        model.addAttribute("productDTOlist", productDTOList);
        return "homepage";
    }

    @GetMapping("/product/{productId}")
    public String viewProductGet(@PathVariable(value = "productId") String productId, Model model){
        Optional<ProductDTO> optionalProductDTO = productService.getProductDTOById(productId);
        if(optionalProductDTO.isEmpty()){
            return "404";
        }
        ProductDTO productDTOToGet = optionalProductDTO.get();
        model.addAttribute("productDTOToGet", productDTOToGet);
        ChosenProductDTO chosenProductDTO = new ChosenProductDTO();
        model.addAttribute("chosenProductDTO", chosenProductDTO);
        return "viewProduct";
    }

    @PostMapping("/product/{productId}")
    public String viewProductPost(@PathVariable(value = "productId") String productId, Model model,
                                  @ModelAttribute ChosenProductDTO chosenProductDTO){
        String loggedInUserEmail = SecurityContextHolder.getContext().getAuthentication().getName();
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        if (auth != null && auth.getAuthorities().stream().anyMatch(a -> a.getAuthority().equals("ROLE_ADMIN"))) {
            productService.addProductStock(chosenProductDTO, productId);
        } else if(auth != null && auth.getAuthorities().stream().anyMatch(a -> a.getAuthority().equals("ROLE_USER"))) {
            shoppingCartService.addToCart(chosenProductDTO, productId, loggedInUserEmail);
        }
        return "redirect:/product/"+productId;
    }

    @PostMapping("/product/remove/{productId}")
    public String removeProductPost(@PathVariable(value = "productId") String productId){
        try {
            productService.removeProductById(productId);
        } catch (ClassNotFoundException e) {
            return "404";
        }
        return "redirect:/home";
    }

    @GetMapping("/register")
    public String registerGet(Model model){
        UserDTO userDTO = new UserDTO();
        model.addAttribute("userDTO", userDTO);
        return "register";
    }

    @PostMapping("/register")
    public String registerPost(@ModelAttribute UserDTO userDTO, BindingResult bindingResult,
                               RedirectAttributes redirectAttributes){
        userValidator.validate(userDTO, bindingResult);
        if(bindingResult.hasErrors()){
            return "register";
        }
        userService.addUser(userDTO);
        redirectAttributes.addFlashAttribute("userAddedSuccessfully", "You have successfully registered, please login!");
        return "redirect:/login";
    }

    @GetMapping("/login")
    public String loginGet(Model model, @ModelAttribute("userAddedSuccessfully") String userAddedSuccessfully){
        model.addAttribute("userAddedSuccessfully", userAddedSuccessfully);
        return "login";
    }

    @GetMapping("/cart")
    public String cartGet(Model model){
        String loggedInUserEmail = SecurityContextHolder.getContext().getAuthentication().getName();
        ShoppingCartDTO shoppingCartDTO = shoppingCartService.getShoppingCartDTOByUserEmail(loggedInUserEmail);
        model.addAttribute("shoppingCartDTO", shoppingCartDTO);
        return "cart";
    }

    @PostMapping("/cart/remove/{chosenProductId}")
    public String cartRemoveItem(@PathVariable (value = "chosenProductId") String chosenProductId){
        shoppingCartService.removeShoppingCartItem(chosenProductId);
        return "redirect:/cart";
    }

    @GetMapping("/checkout")
    public String checkoutGet(Model model){
        String loggedInUserEmail = SecurityContextHolder.getContext().getAuthentication().getName();

        ShoppingCartDTO shoppingCartDTO = shoppingCartService.getShoppingCartDTOByUserEmail(loggedInUserEmail);
        model.addAttribute("shoppingCartDTO", shoppingCartDTO);

        UserDetailsDTO userDetailsDTO = userService.getUserDetailsDTOByEmail(loggedInUserEmail);
        model.addAttribute("userDetailsDTO", userDetailsDTO);

        return "checkout";
    }

    @PostMapping("/sendOrder")
    public String sendOrderPost(@ModelAttribute("userDetailsDTO") UserDetailsDTO userDetailsDTO){
        String loggedInUserEmail = SecurityContextHolder.getContext().getAuthentication().getName();

        customerOrderService.addCustomerOrder(loggedInUserEmail, userDetailsDTO.getShippingAddress());

        return "confirmation";
    }

    @GetMapping("/ordersTracking")
    public String ordersTrackingGet(Model model){
        List<CustomerOrderDTO> customerOrderDTOList = customerOrderService.getAllCustomerDTOs();
        model.addAttribute("customerOrderDTOList", customerOrderDTOList);
        return "ordersTracking";
    }

    @GetMapping("/myOrders")
    public String myOrdersGet(Model model){
        String loggedInUserEmail = SecurityContextHolder.getContext().getAuthentication().getName();

        List<CustomerOrderDTO> customerOrderDTOList = customerOrderService.getCustomerOrderDTOsByClientEmail(loggedInUserEmail);

        model.addAttribute("customerOrderDTOList", customerOrderDTOList);
        return "myOrders";
    }

    @GetMapping("/denied")
    public String deniedGet (){
        return "403";
    }
}
