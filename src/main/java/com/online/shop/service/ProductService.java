package com.online.shop.service;

import com.online.shop.dto.ChosenProductDTO;
import com.online.shop.dto.ProductDTO;
import com.online.shop.entities.Product;
import com.online.shop.mapper.ProductMapper;
import com.online.shop.repository.ProductRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
@Slf4j
public class ProductService {

    @Autowired
    private ProductRepository productRepository;

    @Autowired
    private ProductMapper productMapper;

    public void addProduct(ProductDTO productDTO, MultipartFile multipartFile) {
        Product product = productMapper.map(productDTO, multipartFile);
        productRepository.save(product);
    }

    public void addProductStock(ChosenProductDTO chosenProductDTO, String productID){
        Optional<Product> optionalProduct = productRepository.findById(Integer.valueOf(productID));
        if(optionalProduct.isEmpty()){
            log.error("Product with id = {} doesn't exist", productID);
        } else {
            Product product = optionalProduct.get();
            product.setQuantity(product.getQuantity()+ Integer.parseInt(chosenProductDTO.getQuantity()));
            productRepository.save(product);
        }
    }

    public List<ProductDTO> getAllProductDTOs() {
        List<ProductDTO> productDTOList = new ArrayList<>();
        List<Product> productList = productRepository.findAllByAvailableTrue();
        for (Product product : productList) {
            ProductDTO productDTO = productMapper.map(product);
            productDTOList.add(productDTO);
        }
        return productDTOList;
    }

    public Optional<ProductDTO> getProductDTOById(String productId) {
        Optional<Product> optionalProduct = productRepository.findById(Integer.valueOf(productId));
        if(optionalProduct.isEmpty()){
            return Optional.empty();
        }
        Product product = optionalProduct.get();
        ProductDTO productDTO = productMapper.map(product);
        return Optional.of(productDTO);
    }

    public void removeProductById(String productId) throws ClassNotFoundException {
        Optional<Product> optionalProduct = productRepository.findById(Integer.valueOf(productId));
        if(optionalProduct.isEmpty()){
            throw new ClassNotFoundException("Product not found");
        } else {
            Product product = optionalProduct.get();
            product.setAvailable(false);
            productRepository.save(product);
        }
    }
}
