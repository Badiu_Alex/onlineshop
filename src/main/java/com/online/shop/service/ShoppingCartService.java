package com.online.shop.service;

import com.online.shop.dto.ChosenProductDTO;
import com.online.shop.dto.ShoppingCartDTO;
import com.online.shop.dto.ShoppingCartItemDTO;
import com.online.shop.entities.ChosenProduct;
import com.online.shop.entities.Product;
import com.online.shop.entities.ShoppingCart;
import com.online.shop.entities.User;
import com.online.shop.repository.ChosenProductRepository;
import com.online.shop.repository.ProductRepository;
import com.online.shop.repository.ShoppingCartRepository;
import com.online.shop.repository.UserRepository;
import org.apache.tomcat.util.codec.binary.Base64;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class ShoppingCartService {

    @Autowired
    private ProductRepository productRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private ChosenProductRepository chosenProductRepository;

    @Autowired
    private ShoppingCartRepository shoppingCartRepository;

    public void addToCart(ChosenProductDTO chosenProductDTO, String productId, String loggedInUserEmail){
        Optional<User> optionalUser = userRepository.findByEmail(loggedInUserEmail);
        ShoppingCart shoppingCart = optionalUser.get().getShoppingCart();
        Optional<ChosenProduct> optionalChosenProduct = chosenProductRepository.findByProductIdAndShoppingCartShoppingCartId(Integer.valueOf(productId), shoppingCart.getShoppingCartId());
        if(optionalChosenProduct.isPresent()){
            ChosenProduct chosenProduct = optionalChosenProduct.get();
            chosenProduct.setChosenQuantity(chosenProduct.getChosenQuantity() + Integer.parseInt(chosenProductDTO.getQuantity()));
            chosenProductRepository.save(chosenProduct);
        } else {
            buildChosenProduct(chosenProductDTO, productId, loggedInUserEmail);
        }
    }

    private void buildChosenProduct(ChosenProductDTO chosenProductDTO, String productId, String loggedInUserEmail) {
        ChosenProduct chosenProduct = new ChosenProduct();
        chosenProduct.setChosenQuantity(Integer.valueOf(chosenProductDTO.getQuantity()));
        Optional<Product> optionalProduct = productRepository.findById(Integer.valueOf(productId));
        chosenProduct.setProduct(optionalProduct.get());
        Optional<User> optionalUser = userRepository.findByEmail(loggedInUserEmail);
        chosenProduct.setShoppingCart(optionalUser.get().getShoppingCart());
        chosenProductRepository.save(chosenProduct);
    }

    @Transactional
    public ShoppingCartDTO getShoppingCartDTOByUserEmail(String loggedInUserEmail) {
        ShoppingCart shoppingCart = shoppingCartRepository.findByUserEmail(loggedInUserEmail);
        List<Integer> list = new ArrayList<>();
        ShoppingCartDTO shoppingCartDTO = new ShoppingCartDTO();
        double subTotal = 0;

        for(ChosenProduct chosenProduct : shoppingCart.getChosenProducts()){
            if(!chosenProduct.getProduct().isAvailable()){
                list.add(chosenProduct.getChosenProductId());
                continue;
            }
            ShoppingCartItemDTO shoppingCartItemDTO = new ShoppingCartItemDTO();
            shoppingCartItemDTO.setChosenProductId(String.valueOf(chosenProduct.getChosenProductId()));
            shoppingCartItemDTO.setName(chosenProduct.getProduct().getName());
            shoppingCartItemDTO.setQuantity(String.valueOf(chosenProduct.getChosenQuantity()));
            shoppingCartItemDTO.setPrice(String.valueOf(chosenProduct.getProduct().getPrice()));
            double auxiliaryPrice = chosenProduct.getChosenQuantity() * chosenProduct.getProduct().getPrice();
            subTotal = subTotal + auxiliaryPrice;
            shoppingCartItemDTO.setTotal(String.valueOf(auxiliaryPrice));
            shoppingCartItemDTO.setImage(Base64.encodeBase64String(chosenProduct.getProduct().getImage()));
            shoppingCartDTO.add(shoppingCartItemDTO);
        }
        shoppingCartDTO.setSubTotal(String.valueOf(subTotal));
        shoppingCartDTO.setTotal(String.valueOf(subTotal+50));
        chosenProductRepository.deleteAllById(list);
        return shoppingCartDTO;
    }

    public void removeShoppingCartItem(String chosenProductId) {
        boolean existsById = chosenProductRepository.existsById(Integer.valueOf(chosenProductId));
        if(existsById){
            chosenProductRepository.deleteById(Integer.valueOf(chosenProductId));
        }
    }
}
