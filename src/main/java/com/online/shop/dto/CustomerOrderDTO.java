package com.online.shop.dto;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.List;

@Getter
@Setter
@ToString
public class CustomerOrderDTO {

    private String orderID;
    private String customerName;
    private List<ChosenProductDTO> chosenProductDTOList;

}
